<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.time.*,java.time.format.DateTimeFormatter,java.util.Date"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Activity</title>
</head>
<body>
  	
  	<%! LocalDateTime now = LocalDateTime.now();
     	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(" yyyy-MM-dd HH:mm:ss");
     	
     	LocalDateTime japan = now.plusHours(1);
     	LocalDateTime germany = now.minusHours(7);
     	
     	String japanDateTime = japan.format(formatter);
     	String germanyDateTime = germany.format(formatter);
     	
 %>
  	
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila:<%=  now.format(formatter) %> </li>
		<li> Japan: <%=  japanDateTime%></li>
		<li> Germany: <%= germanyDateTime%></li>
	</ul>
	
	<%! private int initVar=3;
	private int serviceVar=3;
	private int destroyVar=3;
	
  	public void jspInit(){
    	initVar--;
    	System.out.println("jspInit(): init"+initVar);
  		}
  	public void jspDestroy(){
    	destroyVar--;
    	destroyVar = destroyVar + initVar;
    	System.out.println("jspDestroy(): destroy"+destroyVar);
  	}
  	%>
  	
  	<% 
  	serviceVar--;
  	System.out.println("_jspService(): service"+serviceVar);
  	String content1="content1 : "+initVar;
  	String content2="content2 : "+serviceVar;
  	String content3="content3 : "+destroyVar;
  	%>
  	
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
	
</body>
